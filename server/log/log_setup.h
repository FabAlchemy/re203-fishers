#ifndef FISHERS_LOG_SETUP_H
#define FISHERS_LOG_SETUP_H

#include <stdio.h>

extern FILE *log_file;

/** Set log backup file */
void log_set_file(const char *filename);

/** Get verbosity level from string */
int log_verbosity_level(const char *str);

/** Set logging verbosity */
void log_set_verbosity(int verbosity);

/** Free the memory allocated for the log backup */
void log_free();


#endif //FISHERS_LOG_SETUP_H
