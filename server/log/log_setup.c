#include <pthread.h>

#include "log.h"
#include "log_setup.h"

pthread_mutex_t log_mutex;
FILE *log_file;

const int verbosity_levels[] = {LOG_WARN, LOG_INFO, LOG_DEBUG, LOG_TRACE};
const char *verbosity_strings[] = {"warn", "info", "debug", "trace"};

/** A helper to lock/unlock a mutex pointed by udata */
void pthread_mutex_LockFn(bool lock, void *udata) {
    if (lock)
        pthread_mutex_lock(udata);
    else
        pthread_mutex_unlock(udata);
}

/** Initialize a lock to protect concurrent logs */
__attribute__((constructor)) __attribute_used__
void log_init() {
    pthread_mutex_init(&log_mutex, NULL);
    log_set_lock(pthread_mutex_LockFn, &log_mutex);
}

void log_set_file(const char *filename) {
    log_file = fopen(filename, "w");
    if (log_file) {
        log_add_fp(log_file, LOG_TRACE);
        log_info("Writing logs to %s", filename);
    } else {
        log_error("Could not open log file %s", filename);
    }
}

void log_set_verbosity(int verbosity) {
    if (verbosity > 3)
        verbosity = 3;
    log_warn("Setting log level at %s", verbosity_strings[verbosity]);
    log_set_level(verbosity_levels[verbosity]);
}

void log_free() {
    if (log_file)
        fclose(log_file);
}
