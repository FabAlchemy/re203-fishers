add_library(client client.c client.h client_cmd.h client_cmd.c)
target_link_libraries(client parser)

add_library(server server.c server.h)
