#include <string.h>

#include "../log/log.h"
#include "../utils/regex_const.h"
#include "../utils/string.h"
#include "client_cmd.h"

parser client_parser = NULL;

static struct command client_commands[] = {
        {ping_regex, ping_arg_count, (command_handler) client__ping},
        {get_fishes_continuously_regex, get_fishes_continuously_count,
         (command_handler) client__get_fishes_continuously},
        {get_fishes_regex, get_fishes_count, (command_handler) client__get_fishes},
        {hello_id_regex, hello_id_arg_count, (command_handler) client__hello_id},
        {hello_simple_regex, hello_simple_arg_count, (command_handler) client__hello_simple},
        {log_out_regex, log_out_arg_count, (command_handler) client__log_out},
        {add_fish_regex, add_fish_arg_count, (command_handler) client__add_fish},
        {del_fish_regex, del_fish_arg_count, (command_handler) client__del_fish},
        {start_fish_regex, start_fish_arg_count, (command_handler) client__start_fish},
};

__attribute__((constructor)) __attribute_used__ void init() {
    client_parser = parser__init(client_commands, sizeof(client_commands) / sizeof(client_commands[0]));
}

__attribute__((destructor)) __attribute_used__ void clean() {
    parser__free(client_parser);
}

static inline ssize_t send_OK(client c) {
    return send(c->socket, "OK\n", 3, 0);
}

static inline ssize_t send_NOK(client c) {
    return send(c->socket, "NOK\n", 4, 0);
}


int client__hello_dispatch(client c, char **args) {
    if (c->view_id == 0) {
        int view_id = args ? strtoi(args[0]) : 0;
        view_id = aq__request_view(aq, view_id);

        if (view_id != -1) {
            c->view_id = view_id;
            string rep = alloc_printf(hello_success_answer, view_id);
            send(c->socket, rep->content, rep->length, 0);
            string__free(rep);
            return 0;
        }
    }

    send(c->socket, hello_fail_answer, strlen(hello_fail_answer), 0);
    return 0;
}

int client__hello_simple(client c, __attribute__((unused)) char **args) {
    log_info("Received a simple hello");
    return client__hello_dispatch(c, NULL);
}

int client__hello_id(client c, char **args) {
    log_info("Received a hello with id %s", args[0]);
    return client__hello_dispatch(c, args);
}

int client__log_out(client c, __attribute__((unused)) char **args) {
    log_info("Logging out client");
    send(c->socket, "bye\n", 4, 0);
    client__disconnect(c);
    return 0;
}


int client__add_fish(client c, char **args) {
    int_pair pos = {strtoi(args[1]), strtoi(args[2])};
    int_pair dim = {strtoi(args[3]), strtoi(args[4])};

    if (aq__add_fish(aq, c->view_id, args[0], pos, dim, args[5])) {
        log_info("Added fish \"%s\"", args[0]);
        send_OK(c);
    } else {
        log_info("Could not add fish \"%s\"", args[0]);
        send_NOK(c);
    }

    return 0;
}

int client__del_fish(client c, char **args) {
    if (aq__del_fish(aq, args[0])) {
        log_info("Successfully deleted fish \"%s\"", args[0]);
        send_OK(c);
    } else {
        log_info("Could not delete fish \"%s\"", args[0]);
        send_NOK(c);
    }
    return 0;
}

int client__start_fish(client c, char **args) {
    if (aq__start_fish(aq, args[0])) {
        log_info("Successfully started fish \"%s\"", args[0]);
        send_OK(c);
    } else {
        log_info("Could not start fish \"%s\"", args[0]);
        send_NOK(c);
    }
    return 0;
}


int client__ping(client c, char **args) {
    log_trace("Received a ping");
    string res = alloc_printf("pong %s\n", args[0]);
    send(c->socket, res->content, res->length, 0);
    string__free(res);
    return 0;
}

int client__get_fishes(client c, __attribute__((unused)) char **args) {
    if (c->view_id == 0) {
        log_error("A client with no view requested getFishes");
        send_NOK(c);
        return 1;
    }

    log_trace("Received request for getFishes");
    string res = aq__get_fishes_in_view(aq, c->view_id);
    send(c->socket, res->content, res->length, 0);
    string__free(res);
    return 0;
}

int client__get_fishes_continuously(client c, char **args) {

    if (c->watching || c->view_id == 0) {
        log_warn("A client tried to watch the aquarium but is in a wrong state");
        send_NOK(c);
        return 1;
    }

    log_info("A client began to watch the aquarium");
    c->watching = true;
    pthread_create(&c->watcher, NULL, (void *(*)(void *)) client__watcher_thread, c);
    return 0;
}