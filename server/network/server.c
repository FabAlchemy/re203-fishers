#include <stdlib.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <poll.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>

#include "../log/log.h"
#include "../utils/queue.h"
#include "server.h"
#include "client.h"

/** The internal representation of a server */
struct server {
    int port; /**< The listening port */
    int socket; /**< The server socket file descriptor */
    int client_timeout; /**< Maximum delay between two requests before disconnection */
    STAILQ_HEAD(client_list, client) clients; /**< A list of the connected clients */
    pthread_t accept_thread; /**< The thread executing the accept() function */
    bool running; /**< Is the server running? */
    pthread_mutex_t mutex; /**< A mutex to protect `running` */
};

server server__init(int port, int client_timeout) {
    server s = calloc(1, sizeof(struct server));
    STAILQ_INIT(&s->clients);
    s->running = false;
    s->port = port;
    s->client_timeout = client_timeout;
    pthread_mutex_init(&s->mutex, NULL);
    return s;
}

/** Accept a new client and launch a new client */
void server__accept(server s) {
    // TODO: Get the address of the client
    int client_socket = accept(s->socket, NULL, NULL);

    if (client_socket != -1) {
        log_info("Accepting incoming connection %d", client_socket);
        struct client *client = client__new(client_socket);
        STAILQ_INSERT_TAIL(&s->clients, client, next);
    } else {
        log_error("Could not accept incoming connection %d: %s", client_socket, strerror(errno));
    }
}

/** Join and remove disconnected clients */
void server__clean_disconnected_clients(server s) {
    if (!STAILQ_EMPTY(&s->clients)) {
        struct client *client;
        struct client *next_client;
        STAILQ_FOREACH_SAFE(client, &s->clients, next, next_client) {
            pthread_mutex_lock(&client->mutex);
            bool client_connected = client->connected;
            time_t last_seen = client->last_seen;
            pthread_mutex_unlock(&client->mutex);

            if (time(NULL) - last_seen > s->client_timeout) {
                log_warn("Client was away for too long, disconnecting");
                client__disconnect(client);
                client_connected = false;
            }

            if (!client_connected) {
                STAILQ_REMOVE(&s->clients, client, client, next);
                client__join(client);
            }
        }
    }
}

/** Handle new connections */
void *server__thread(server s) {
    struct pollfd poll_fd = {.fd = s->socket, .events = POLLIN};
    bool running = true;

    while (running) {
        server__clean_disconnected_clients(s);

        if (poll(&poll_fd, 1, 1000) == -1 || poll_fd.revents & (POLLNVAL | POLLERR | POLLHUP)) {
            log_error("Error while polling socket: %s", strerror(errno));
        }

        if (poll_fd.revents & (POLLIN)) {
            server__accept(s);
        }

        pthread_mutex_lock(&s->mutex);
        running = s->running;
        pthread_mutex_unlock(&s->mutex);
    }

    return NULL;
}

void server__start(server s) {
    s->running = true;

    s->socket = socket(AF_INET, SOCK_STREAM, 0);
    if (s->socket == -1) {
        log_fatal("Could not create socket: %s", strerror(errno));
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_addr;
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(s->port);

    log_warn("Starting server on port %d", s->port);

    // Set the REUSEADDR option to bypass the delay when restarting the server
    if (setsockopt(s->socket, SOL_SOCKET, SO_REUSEADDR, &(int) {1}, sizeof(int)) == -1) {
        log_error("Could not set REUSEADDR option");
    }

    if (bind(s->socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) == -1) {
        log_fatal("Could not bind socket to local address: %s", strerror(errno));
        exit(EXIT_FAILURE);
    }

    listen(s->socket, SOCKET_QUEUE_LEN);
    log_info("Created and bound socket to local address");

    pthread_create(&s->accept_thread, NULL, (void *(*)(void *)) server__thread, s);
    log_info("Spawned connection handler thread");
}

void server__stop(server s) {
    log_trace("Stopping server");

    pthread_mutex_lock(&s->mutex);
    s->running = false;
    pthread_mutex_unlock(&s->mutex);

    pthread_join(s->accept_thread, NULL);
    log_info("Disconnecting clients");

    if (!STAILQ_EMPTY(&s->clients)) {
        struct client *client;
        struct client *next_client;
        STAILQ_FOREACH_SAFE(client, &s->clients, next, next_client) {
            STAILQ_REMOVE(&s->clients, client, client, next);
            client__disconnect(client);
            client__join(client);
        }
    }

    log_info("Server stopped");
    close(s->socket);
}

void server__free(server s) {
    log_trace("Freeing server");
    server__stop(s);
    free(s);
}