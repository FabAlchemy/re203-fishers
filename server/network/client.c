#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <poll.h>
#include <unistd.h>
#include <errno.h>

#include "../log/log.h"
#include "../parser/parser.h"
#include "../aquarium/aquarium.h"
#include "client.h"
#include "client_cmd.h"

#define BUFFER_SIZE 1024

client client__new(int socket) {
    client c = calloc(1, sizeof(struct client));
    c->socket = socket;
    c->last_seen = time(NULL);
    c->connected = true;
    pthread_mutex_init(&c->mutex, NULL);
    pthread_cond_init(&c->cond, NULL);

    pthread_create(&c->handler, NULL, (void *(*)(void *)) client__handler, c);
    return c;
}

/** Receive a message from the client socket */
void client__recv(client c) {
    char buffer[BUFFER_SIZE];

    int read_size = recv(c->socket, buffer, BUFFER_SIZE, 0);
    if (read_size > 0) {
        buffer[read_size] = '\0';
        char *strtok_context;
        char *cmd = NULL;
        do {
            cmd = strtok_r(cmd == NULL ? buffer : NULL, "\n", &strtok_context);
            if (cmd)
                parser__parse(client_parser, cmd, c);
        } while (cmd);
    } else if (read_size == 0) {
        log_warn("Client closed the connection");
        client__disconnect(c);

    } else if (read_size == -1) {
        log_error("Error while receiving a message: %s", strerror(errno));
    }
}

void client__handler(client c) {
    bool connected = true;
    struct pollfd poll_fd = {.fd = c->socket, .events = POLLIN};

    while (connected) {
        int poll_res = poll(&poll_fd, 1, 2000);

        if (poll_res == -1 || poll_fd.revents & (POLLNVAL | POLLERR)) {
            log_trace("Error while polling socket: %s", strerror(errno));
        }

        if (poll_fd.revents & POLLHUP) {
            log_warn("Client disconnected");
            return;
        }

        if (poll_fd.revents & POLLIN) {
            client__recv(c);

            pthread_mutex_lock(&c->mutex);
            c->last_seen = time(NULL);
            pthread_mutex_unlock(&c->mutex);
        }

        pthread_mutex_lock(&c->mutex);
        connected = c->connected;
        pthread_mutex_unlock(&c->mutex);
    }
}

void client__watcher_thread(client c) {
    bool connected = true;

    // Synchronize the clients
    sleep(aq__get_time_interval(aq) - time(NULL) % aq__get_time_interval(aq));

    while (connected) {
        string res = aq__get_fishes_in_view(aq, c->view_id);
        send(c->socket, res->content, res->length, 0);
        string__free(res);

        pthread_mutex_lock(&c->mutex);
        pthread_cond_timedwait(&c->cond, &c->mutex, &(struct timespec) {time(NULL) + aq__get_time_interval(aq)});
        connected = c->connected;
        pthread_mutex_unlock(&c->mutex);
    }
}

void client__disconnect(client c) {
    log_info("Disconnecting client");

    pthread_mutex_lock(&c->mutex);
    c->connected = false;
    pthread_cond_broadcast(&c->cond);
    pthread_mutex_unlock(&c->mutex);


    if (c->view_id)
        aq__yield_view(aq, c->view_id);

    shutdown(c->socket, SHUT_RDWR);
    close(c->socket);
}

void client__join(client c) {
    if (c->watching)
        pthread_join(c->watcher, NULL);

    pthread_join(c->handler, NULL);
    free(c);
}