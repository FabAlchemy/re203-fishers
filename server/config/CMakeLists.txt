add_library(config config.c config.h)
target_link_libraries(config parser)

add_executable(config_test config_test.c)
target_link_libraries(config_test cmocka)
target_link_libraries(config_test config)
set_target_properties(config_test PROPERTIES LINK_FLAGS "-Wl,--wrap=fopen")

add_test(NAME config_test COMMAND config_test)