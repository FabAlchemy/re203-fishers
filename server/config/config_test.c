#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdio.h>
#include <string.h>
#include "config.h"

/** A mock configuration file to be used instead of a real file */
char *mock_config_file = "# This is a mock configuration file\n"
                         "first-attribute = 42\n"
                         "# Another comment\n"
                         "second-attribute = 1\n"
                         "third-attribute = ./abcde";

/** Replace the fopen function by mapping the string `file` in memory */
__attribute__((unused))
FILE *__wrap_fopen(const char *file, const char *modes) { // NOLINT
    return fmemopen((void *) file, strlen(mock_config_file), modes);
}

/**
 * TEST: The config should be able to retrieve two attributes from a file
 */
static void config__finds_attributes(__attribute__((__unused__)) void **state) {
    config c = config__init(mock_config_file);
    assert_ptr_not_equal(c, NULL);

    char *res;

    res = config__get_value(c, "first-attribute");
    assert_string_equal(res, "42");

    res = config__get_value(c, "second-attribute");
    assert_string_equal(res, "1");

    res = config__get_value(c, "third-attribute");
    assert_string_equal(res, "./abcde");

    config__free(c);
}

/**
 * TEST: The config should prevent access to unknown key
 */
static void config__prevents_unknown_key(__attribute__((__unused__)) void **state) {
    config c = config__init(mock_config_file);
    assert_ptr_not_equal(c, NULL);

    char *res = config__get_value(c, "abc");
    assert_ptr_equal(res, NULL);

    config__free(c);
}

int main() {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(config__finds_attributes),
            cmocka_unit_test(config__prevents_unknown_key),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}