#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "../parser/parser.h"
#include "../utils/string.h"
#include "../log/log.h"
#include "config.h"

#define CONFIG_INIT_CAPACITY 10

/** Internal representation of a configuration */
struct config {
    int key_count; /**< The number of key-value pairs */
    int capacity;  /**< The size of the key-value arrays */
    string *keys; /**< The array of keys */
    string *values; /**< The array of values */
};


/** Find the index of a key in the config  */
int config__get_key_index(config c, char *key) {
    for (int k = 0; k < c->key_count; k++)
        if (strcmp(key, c->keys[k]->content) == 0)
            return k;

    return -1;
}

/**
 * Set the value of a key
 * @note This is a parsing command handler
 * @param args[0] key
 * @param args[1] value
 * @return 0 on success, -1 if the key is unknown
 */
int config__set_value(config c, char **args) {
    int k = config__get_key_index(c, args[0]);

    if (k != -1) {
        log_info("Found duplicate configuration key \"%s\"", args[0]);
        string__free(c->values[k]);
        c->values[k] = string__from(args[1]);
        return 0;
    }

    if (c->key_count >= c->capacity) {
        c->capacity *= 2;
        if (!realloc(c->keys, c->capacity * sizeof(char *))
            || !realloc(c->values, c->capacity * sizeof(char *)))
            return -1;
    }

    c->keys[c->key_count] = string__from(args[0]);
    c->values[c->key_count] = string__from(args[1]);
    c->key_count++;

    return 0;
}


char *config__get_value(config c, char *key) {
    int k = config__get_key_index(c, key);

    if (k != -1)
        return c->values[k]->content;

    return NULL;
}

/** Extract values from a config file
 * @param c an initialized config
 * @param file the config file to parse
 * @return 0 on success, -1 on error
 */
int config__parse_file(config c, char *file) {
    log_info("Parsing configuration from file \"%s\"", file);

    FILE *config_file = fopen(file, "r");

    if (!config_file) {
        log_error("Could not open file \"%s\": %s", file, strerror(errno));
        return -1;
    }

    struct command cmd[] = {
            {"([a-z-]+) = ([[:print:]]+)", 2, (command_handler) config__set_value},
    };

    parser p = parser__init(cmd, sizeof(cmd) / sizeof(cmd[0]));

    char *line_buffer = NULL;
    size_t buffer_size = 0;
    ssize_t read_count;
    int res = 0;

    while ((read_count = getline(&line_buffer, &buffer_size, config_file)) != -1 && res == 0)
        if (read_count > 2 && line_buffer[0] != '#') {
            res = parser__parse(p, line_buffer, c);
        }

    if (line_buffer)
        free(line_buffer);
    fclose(config_file);
    parser__free(p);

    if (res == -1)
        log_error("Configuration parsing failed");

    return res;
}

config config__init(char *file) {
    log_trace("Initializing a configuration");

    config c = malloc(sizeof(*c));
    c->key_count = 0;
    c->capacity = CONFIG_INIT_CAPACITY;

    c->keys = calloc(c->capacity, sizeof(string *));
    c->values = calloc(c->capacity, sizeof(string *));

    if (config__parse_file(c, file) != 0) {
        config__free(c);
        return NULL;
    }

    return c;
}

void config__free(config c) {
    log_trace("Freeing a configuration");

    for (int k = 0; k < c->key_count; k++) {
        string__free(c->keys[k]);
        string__free(c->values[k]);
    }

    free(c->keys);
    free(c->values);
    free(c);
}