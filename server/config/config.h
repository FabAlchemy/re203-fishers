#ifndef FISHERS_CONFIG_H
#define FISHERS_CONFIG_H

/** An opaque structure representing a configuration */
typedef struct config *config;

/**
 * Extract configuration data from file
 * @param file the path to the configuration file to parse
 * @param expected_keys an array of expected keys
 * @param key_count the number of expected keys
 */
config config__init(char *file);

/**
 * Retrieve a value from the configuration
 * @param c the config to fetch the data from
 * @param key a known key
 * @return the value associated to the provided key
 */
char *config__get_value(config c, char *key);

/**
 * Free the allocated configuration
 * @param c the config to free
 */
void config__free(config c);


#endif // FISHERS_CONFIG_H
