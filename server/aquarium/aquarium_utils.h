#ifndef FISHERS_AQUARIUM_UTILS_H
#define FISHERS_AQUARIUM_UTILS_H

#include <math.h>
#include <string.h>

#include "fish.h"
#include "view.h"
#include "aquarium.h"

/**
 * Searches for the view view_id into the aquarium aq
 * @return the pointer to the view if found, NULL otherwise
 */
static inline view find_view(aquarium aq, int view_id) {
    pthread_mutex_lock(&aq->views_mutex);

    view viewp;
    STAILQ_FOREACH(viewp, &aq->views, next) {
        if (view_id == viewp->id) {
            pthread_mutex_unlock(&aq->views_mutex);
            return viewp;
        }
    }

    pthread_mutex_unlock(&aq->views_mutex);
    return NULL;
}

/**
 * Searches for a inactive view in the aquarium aq
 * @return the pointer to the view if found, NULL otherwise
 */
static inline view find_inactive_view(aquarium aq) {
    pthread_mutex_lock(&aq->views_mutex);

    view viewp;
    STAILQ_FOREACH(viewp, &aq->views, next) {
        if (!viewp->active) {
            pthread_mutex_unlock(&aq->views_mutex);
            return viewp;
        }
    }

    pthread_mutex_unlock(&aq->views_mutex);
    return NULL;
}

/**
 * Searches for the fish named name into the aquarium aq
 * @return the pointer to the fish if found, NULL otherwise
 */
static inline fish find_fish(aquarium aq, char *name) {
    pthread_mutex_lock(&aq->fishes_mutex);

    fish fishp;
    STAILQ_FOREACH(fishp, &aq->fishes, next) {
        if (strcmp(fishp->name, name) == 0) {
            pthread_mutex_unlock(&aq->fishes_mutex);
            return fishp;
        }
    }

    pthread_mutex_unlock(&aq->fishes_mutex);
    return NULL;
}

/**
 * Indicates if a number is a percentage
 * @return true if the condition above is satisfied, false otherwise
 */
static inline bool is_percentage(int value) {
    return (value >= 0 && value <= 100);
}

/**
 * Indicates if a fish's path intersects a view
 */
static inline bool is_fish_path_in_view(view v, fish f) {
    rectangle prev = rectangle__new_at(f->prev_pos, f->dim);
    rectangle curr = rectangle__new_at(f->curr_pos, f->dim);
    rectangle next = rectangle__new_at(f->next_pos, f->dim);

    rectangle traj_prev_curr = rectangle__wrapping(prev, curr);
    rectangle traj_curr_next = rectangle__wrapping(curr, next);

    return !rectangle__is_intersection_empty(rectangle__new_at(v->pos, v->dim), traj_prev_curr)
           || !rectangle__is_intersection_empty(rectangle__new_at(v->pos, v->dim), traj_curr_next);
}

/** Normalizes the number target according to reference */
static inline int normalize(int target, int reference) {
    return round((double) (target * 100) / reference);
}

/** Denormalizes the number target according to reference */
static inline int denormalize(int target, int reference) {
    return round((double) (target * reference) / 100);
}


#endif // FISHERS_AQUARIUM_UTILS_H
