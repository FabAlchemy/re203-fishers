#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "../log/log.h"
#include "aquarium.h"
#include "fish.h"
#include "view.h"

aquarium aq__init(int time_interval) {
    log_info("Initializing an aquarium");
    aquarium aq = malloc(sizeof(*aq));

    aq->time_interval = time_interval;
    aq->updating = false;

    STAILQ_INIT(&aq->fishes);
    STAILQ_INIT(&aq->views);

    pthread_mutex_init(&aq->fishes_mutex, NULL);
    pthread_mutex_init(&aq->views_mutex, NULL);
    pthread_mutex_init(&aq->updating_mutex, NULL);
    pthread_cond_init(&aq->updating_cond, NULL);

    return aq;
}

void aq__clean(aquarium aq) {
    log_info("Cleaning an aquarium");

    aq__stop(aq);

    pthread_mutex_lock(&aq->fishes_mutex);
    fish fishp, fish_tmp;
    STAILQ_FOREACH_SAFE(fishp, &aq->fishes, next, fish_tmp) {
        STAILQ_REMOVE(&aq->fishes, fishp, fish, next);
        fish__free(fishp);
    }
    pthread_mutex_unlock(&aq->fishes_mutex);

    pthread_mutex_lock(&aq->views_mutex);
    view viewp, view_tmp;
    STAILQ_FOREACH_SAFE(viewp, &aq->views, next, view_tmp) {
        STAILQ_REMOVE(&aq->views, viewp, view, next);
        view__free(viewp);
    }
    pthread_mutex_unlock(&aq->views_mutex);
}

void aq__free(aquarium aq) {
    log_info("Freeing an aquarium");

    aq__clean(aq);
    free(aq);
}

int aq__get_time_interval(aquarium aq) {
    return aq->time_interval;
}

void aq__update_thread(aquarium aq) {
    bool updating = true;

    while (updating) {
        aq__update(aq);

        pthread_mutex_lock(&aq->updating_mutex);
        pthread_cond_timedwait(&aq->updating_cond, &aq->updating_mutex,
                               &(struct timespec) {time(NULL) + aq->time_interval});
        updating = aq->updating;
        pthread_mutex_unlock(&aq->updating_mutex);
    }
}

void aq__start(aquarium aq) {
    bool should_start = true;

    pthread_mutex_lock(&aq->updating_mutex);
    if (aq->updating) {
        should_start = false;
    } else {
        aq->updating = true;
    }
    pthread_mutex_unlock(&aq->updating_mutex);

    if (should_start) {
        log_warn("Starting aquarium updates");
        pthread_create(&aq->updater, NULL, (void *(*)(void *)) aq__update_thread, aq);
    } else {
        log_warn("The aquarium is already updating");
    }
}

void aq__stop(aquarium aq) {
    bool should_stop = false;

    pthread_mutex_lock(&aq->updating_mutex);
    if (aq->updating) {
        aq->updating = false;
        should_stop = true;
    }
    pthread_cond_broadcast(&aq->updating_cond);
    pthread_mutex_unlock(&aq->updating_mutex);

    if (should_stop) {
        log_info("Stopping aquarium updates");
        pthread_join(aq->updater, NULL);
    } else {
        log_info("The aquarium was not updating");
    }
}

void aq__update(aquarium aq) {
    fish fishp;

    pthread_mutex_lock(&aq->fishes_mutex);
    if (!STAILQ_EMPTY(&aq->fishes)) {
        STAILQ_FOREACH(fishp, &aq->fishes, next) {
            if (fishp->is_started)
                fish__move(fishp, aq->dim, aq->time_interval);
        }
    }
    pthread_mutex_unlock(&aq->fishes_mutex);
}

void aq__show(aquarium aq, FILE *stream) {
    fprintf(stream, "%dx%d\n", aq->dim.x, aq->dim.y);

    pthread_mutex_lock(&aq->views_mutex);

    if (STAILQ_EMPTY(&aq->views))
        fprintf(stream, "No views\n");
    else {
        view vp;
        STAILQ_FOREACH(vp, &aq->views, next) {
            view__show(vp, stream);
        }
    }

    pthread_mutex_unlock(&aq->views_mutex);
}

void aq__print_fishes(aquarium aq) {
    printf("Aquarium's fishes:\n");

    pthread_mutex_lock(&aq->fishes_mutex);

    fish fishp;
    int cpt = 0;
    STAILQ_FOREACH(fishp, &aq->fishes, next) {
        printf("> %d: %s\n", cpt, fishp->name);
        cpt++;
    }

    pthread_mutex_unlock(&aq->fishes_mutex);

    if (!cpt)
        printf("EMPTY\n");
    printf("\n");
}
