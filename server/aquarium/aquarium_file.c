#include <errno.h>
#include <string.h>

#include "../log/log.h"
#include "../parser/parser.h"
#include "../utils/regex_const.h"
#include "aquarium.h"

#define aq_size_regex NUMBER "x" NUMBER END
#define view_details_regex "N" NUMBER SPACE NUMBER "x" NUMBER "\\+" NUMBER "\\+" NUMBER END

/**
 * Load aquarium size
 * @param args[0] width
 * @param args[1] height
 */
int aq__load_size(aquarium aq, char **args) {
    int_pair aq_dim = (int_pair) {strtoi(args[0]), strtoi(args[1])};

    log_info("Setting aquarium size to %dx%d", aq_dim.x, aq_dim.y);
    if (!int_pair__is_positive(aq_dim)) {
        log_error("Topology file has negative aquarium size values");
        return -1;
    }

    aq->dim = aq_dim;
    return 0;
}

/**
 * Load a view ID, size and position
 * @param args[0] id
 * @param args[1], args[2] coordinates
 * @param args[3], args[4] dimensions
 */
int aq__load_view(aquarium aq, char **args) {
    int id = strtoi(args[0]);
    int_pair view_pos = (int_pair) {strtoi(args[1]), strtoi(args[2])};
    int_pair view_dim = (int_pair) {strtoi(args[3]), strtoi(args[4])};
    if (aq__add_view(aq, id, view_pos, view_dim))
        return 0;
    return -1;
}


int aq__load(aquarium aq, char *file) {

    FILE *topology_file = fopen(file, "r");

    if (topology_file)
        log_warn("Parsing topology from file \"%s\"", file);
    else {
        log_error("Could not open file \"%s\": %s", file, strerror(errno));
        return -1;
    }

    static struct command cmd[] = {
            {view_details_regex, 5, (command_handler) aq__load_view},
            {aq_size_regex,      2, (command_handler) aq__load_size},
    };

    parser p = parser__init(cmd, 2);

    char *line_buffer = NULL;
    size_t buffer_size = 0;
    ssize_t read_count;
    int res = 0;

    while ((read_count = getline(&line_buffer, &buffer_size, topology_file)) != -1 && res == 0)
        if (read_count > 2) {
            res = parser__parse(p, line_buffer, aq);
        }

    if (line_buffer)
        free(line_buffer);
    fclose(topology_file);
    parser__free(p);

    if (res == -1)
        log_error("Topology parsing failed");

    return res;
}

int aq__save(aquarium aq, char *file) {

    FILE *topology_file = fopen(file, "w");

    if (topology_file)
        log_warn("Writing topology to %s", file);
    else {
        log_error("Could not open file \"%s\": %s", file, strerror(errno));
        return -1;
    }

    aq__show(aq, topology_file);

    fclose(topology_file);
    return 0;
}
