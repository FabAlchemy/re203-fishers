#ifndef FISH_BEHAVIOR_H
#define FISH_BEHAVIOR_H

#include "fish.h"
#include "aquarium.h"

/**
 * Finds the function pointer corresponding to wanted_behavior
 * @return a pointer to a function, NULL otherwise
 */
fish_behavior fish_behavior__find(char *wanted_behavior);

/**
 * Computes the next position following a random path behavior and a given time_interval allowed
 * @return the next position
 */
int_pair fish_behavior__random(fish f, int_pair aq_dim, int time_interval);

/**
 * Computes the next position following a horizontal path behavior and a given time_interval allowed
 * @return the next position
 */
int_pair fish_behavior__horizontal(fish f, int_pair aq_dim, int time_interval);


#endif