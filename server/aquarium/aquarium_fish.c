#include "../log/log.h"

#include "aquarium.h"
#include "aquarium_utils.h"

bool aq__add_fish(aquarium aq, int view_id, char *name, int_pair pos, int_pair dim, char *behavior) {
    log_info("Adding a %s fish %s in view %d, at %dx%d, size: %dx%d",
             behavior, name, view_id, pos.x, pos.y, dim.x, dim.y);

    if (find_fish(aq, name)) {
        log_warn("Failed to add fish %s: name already in use", name);
        return false;
    }

    view v = find_view(aq, view_id);
    if (!v) {
        log_warn("Failed to add fish %s: view %d does not exist", name, view_id);
        return false;
    }

    if (!is_percentage(dim.x) || !is_percentage(dim.y) || !is_percentage(pos.x) || !is_percentage(pos.y)) {
        log_warn("Failed to add fish %s: one component of size or position is not valid", name);
        return false;
    }

    // Position of the fish is given regarding the view: thus it must be translated after being denormalized
    int_pair abs_pos = {v->pos.x + denormalize(pos.x, v->dim.x),
                        v->pos.y + denormalize(pos.y, v->dim.y)};
    int_pair abs_dim = {denormalize(dim.x, v->dim.x), denormalize(dim.y, v->dim.y)};

    fish f = fish__new(name, abs_pos, abs_dim, behavior);
    if (!f) {
        log_warn("Failed to add fish %s: unknown behavior", name);
        return false;
    }

    pthread_mutex_lock(&aq->fishes_mutex);
    STAILQ_INSERT_HEAD(&aq->fishes, f, next);
    pthread_mutex_unlock(&aq->fishes_mutex);

    return true;
}

bool aq__del_fish(aquarium aq, char *name) {
    log_info("Deleting fish %s", name);

    fish f = find_fish(aq, name);
    if (!f) {
        log_warn("Failed to delete fish %s: it does not exist", name);
        return false;
    }

    pthread_mutex_lock(&aq->fishes_mutex);
    STAILQ_REMOVE(&aq->fishes, f, fish, next);
    pthread_mutex_unlock(&aq->fishes_mutex);

    fish__free(f);
    return true;
}

bool aq__start_fish(aquarium aq, char *name) {
    log_info("Starting fish %s", name);

    fish f = find_fish(aq, name);
    if (!f) {
        log_warn("Failed to start fish %s: it does not exist", name);
        return false;
    }

    pthread_mutex_lock(&aq->fishes_mutex);
    fish__start(f);
    for (int i = 0; i < 2; i++)
        fish__move(f, aq->dim, aq->time_interval);
    pthread_mutex_unlock(&aq->fishes_mutex);
    return true;
}

/**
 * Extract fish information in a string
 * @note The coordinates are relative to the view
 */
static string aq__get_fish_in_view(fish f, view v, int time_interval) {
    char *desc_template = " [%s at %dx%d,%dx%d,%d]";

    int x = normalize(f->curr_pos.x - v->pos.x, v->dim.x);
    int y = normalize(f->curr_pos.y - v->pos.y, v->dim.y);
    int width = normalize(f->dim.x, v->dim.x);
    int height = normalize(f->dim.y, v->dim.y);

    return alloc_printf(desc_template, f->name, x, y, width, height, time_interval);
}

string aq__get_fishes_in_view(aquarium aq, int view_id) {
    view v = find_view(aq, view_id);
    if (!v) {
        log_error("Failed to get fishes in view %d: it does not exist", view_id);
        return NULL;
    }

    int list_length = 0;

    STAILQ_HEAD(desc_list, string) desc_list = STAILQ_HEAD_INITIALIZER(desc_list);
    fish fish;

    pthread_mutex_lock(&aq->fishes_mutex);
    STAILQ_FOREACH(fish, &aq->fishes, next) {
        if (is_fish_path_in_view(v, fish)) {
            string desc = aq__get_fish_in_view(fish, v, aq->time_interval);
            STAILQ_INSERT_TAIL(&desc_list, desc, next);
            list_length += desc->length;
        }
    }
    pthread_mutex_unlock(&aq->fishes_mutex);

    // Allocate a string to contain the fish descriptions and "list\n\0"
    int len = list_length + 4 + 1;
    char *list = calloc(len + 1, sizeof(char));
    string res = string__new(list, len);

    strcat(list, "list");
    char *list_ptr = list + 4;

    string desc, next_desc;
    STAILQ_FOREACH_SAFE(desc, &desc_list, next, next_desc) {
        strcat(list_ptr, desc->content);
        list_ptr += desc->length;
        string__free(desc);
    }

    *list_ptr = '\n';

    log_debug("View %d: %s", view_id, list);
    return res;
}
