#ifndef VIEW_H
#define VIEW_H

#include <stdbool.h>

#include "../utils/queue.h"
#include "../utils/geometry.h"

/**
 * An opaque structure representing a view
 */
typedef struct view *view;

/**
 * A view to be stored in a tail queue
 */
struct view {
    int id; /**< the identity number of the view */
    int_pair dim; /**< view size */
    int_pair pos; /**< view position */
    bool active; /**< is the view currently used? */

    STAILQ_ENTRY(view) next; /**< the next view in the tail queue */
};

/**
 * Allocates memory for a vew.
 * @params characteristics of the new view
 * @return on success, a pointer to a new view
 */
view view__new(int id, int_pair pos, int_pair dim);

/** Frees memory space pointed to by v */
void view__free(view v);

/**
 * Prints the characteristics of the view v
 */
void view__show(view v, FILE *stream);

#endif
