#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>

#include "aquarium.h"
#include "view.h"

aquarium aq;
static int time_interval = 1;

#define SIZE 4096

/** A mock configuration file to be used instead of a real file */
char mock_topology_file[SIZE] = "1000x1000\n"
                                "N1 0x0+500+500\n"
                                "N2 500x0+500+500\n"
                                "N3 0x500+500+500\n"
                                "N4 500x500+500+500\n";

/** Replace the fopen function by mapping the string `file` in memory */
__attribute__((unused))
FILE *__wrap_fopen(const char *file, const char *modes) { // NOLINT
    return fmemopen((void *) file, strlen(file), modes);
}

/** A helper to instantiate an aquarium before each test */
int aq_setup(__attribute__((__unused__)) void **state) {
    assert_ptr_equal(aq, NULL);
    aq = aq__init(time_interval);
    aq->dim = (int_pair) {2000, 2000};
    return 0;
}

/** A helper to clean the aq after each test */
int aq_teardown(__attribute__((__unused__)) void **state) {
    assert_ptr_not_equal(aq, NULL);
    aq__free(aq);
    aq = NULL;
    return 0;
}

/** A helper to check the details of the next view and return the one after */
static view check_next_view(view prev_view, int id, int_pair pos, int_pair dim) {
    view v = STAILQ_NEXT(prev_view, next);
    assert_int_equal(v->id, id);
    assert_true(int_pair__equal(v->pos, pos));
    assert_true(int_pair__equal(v->dim, dim));

    return v;
}

/**
 * TEST: The aquarium file should be able to retrieve the topology from a file
 */
static void aq__load_topology(__attribute__((__unused__)) void **state) {
    int res = aq__load(aq, mock_topology_file);
    assert_int_equal(res, 0);

    assert_true(int_pair__equal(aq->dim, (int_pair) {1000, 1000}));

    view first_view = STAILQ_FIRST(&aq->views);
    assert_int_equal(first_view->id, 4);
    assert_true(int_pair__equal(first_view->pos, (int_pair) {500, 500}));
    assert_true(int_pair__equal(first_view->dim, (int_pair) {500, 500}));

    view second_view = check_next_view(first_view, 3, (int_pair) {0, 500}, (int_pair) {500, 500});
    view third_view = check_next_view(second_view, 2, (int_pair) (int_pair) {500, 0}, (int_pair) {500, 500});
    check_next_view(third_view, 1, (int_pair) {0, 0}, (int_pair) {500, 500});
}

/**
 * TEST: The aquarium file should be able to save the topology to a file
 */
static void aq__save_topology(__attribute__((__unused__)) void **state) {
    aq__add_view(aq, 1, (int_pair) {20, 50}, (int_pair) {600, 700});
    aq__add_view(aq, 2, (int_pair) {40, 80}, (int_pair) {300, 200});

    int res = aq__save(aq, mock_topology_file);
    assert_int_equal(res, 0);

    assert_string_equal(mock_topology_file, "2000x2000\n"
                                            "N2 40x80+300+200\n"
                                            "N1 20x50+600+700\n");
}

int main() {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test_setup_teardown(aq__load_topology, aq_setup, aq_teardown),
            cmocka_unit_test_setup_teardown(aq__save_topology, aq_setup, aq_teardown),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}