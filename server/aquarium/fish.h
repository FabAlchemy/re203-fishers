#ifndef FISH_H
#define FISH_H

#include <stdbool.h>

#include "../utils/queue.h"
#include "../utils/geometry.h"

/**
 * An opaque structure representing a fish
 */
typedef struct fish *fish;

/**
 * A type of function to handle the behavior of a fish in an aquarium
 */
typedef int_pair (*fish_behavior)(fish f, int_pair aq_dim, int time_interval);

/**
 * A fish to be stored in a tail queue
 */
struct fish {
    char *name; /**< the name of the fish */
    int_pair dim; /**< the fish size */
    int_pair prev_pos; /**< the previous position */
    int_pair curr_pos; /**< the current position */
    int_pair next_pos; /**< the next position */
    bool is_started; /**< a boolean to indicate whether or not the fish is started */
    fish_behavior behavior; /**< a function determining the behavior of the fish */

    STAILQ_ENTRY(fish) next; /**< the next fish in the tail queue */
};


/**
 * Allocates memory for a new fish
 * @params characteristics of the new fish
 * @return on success, a pointer to a new fish, NULL otherwise
 */
fish fish__new(const char *name, int_pair pos, int_pair dim, char *behavior);

/** Frees memory pointed to by f */
void fish__free(fish f);

/** Starts the fish pointed to by f */
void fish__start(fish f);

/** Executes a move according to behavior, current position, the aquarium dimensions and the given time time_interval */
bool fish__move(fish f, int_pair aq_dim, int time_interval);

/**
 * Debugging function, prints all the content of the fish f (except its behavior)
 * @return a pointer to the allocated memory for the name of the fish
 */
const char *fish__print(fish f);

#endif
