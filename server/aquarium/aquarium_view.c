#include "../utils/queue.h"
#include "../log/log.h"
#include "aquarium.h"
#include "aquarium_utils.h"

bool aq__add_view(aquarium aq, int view_id, int_pair pos, int_pair dim) {
    log_info("Adding view %d at %dx%d, size: %dx%d", view_id, pos.x, pos.y, dim.x, dim.y);

    view v = find_view(aq, view_id);
    if (v) {
        log_warn("Failed to add view %d: id already in use", view_id);
        return false;
    }

    if (!rectangle__is_fitting_in((rectangle) {pos, int_pair__add(pos, dim)},
                                  (rectangle) {(int_pair) {0, 0}, aq->dim})) {
        log_warn("Failed to add view %d: one component of size or position is not valid", view_id);
        return false;
    }

    view nv = view__new(view_id, pos, dim);
    pthread_mutex_lock(&aq->views_mutex);
    STAILQ_INSERT_HEAD(&aq->views, nv, next);
    pthread_mutex_unlock(&aq->views_mutex);
    return true;
}

int aq__request_view(aquarium aq, int view_id) {
    view v = NULL;

    if (view_id)
        v = find_view(aq, view_id);

    if (!v || v->active) {
        v = find_inactive_view(aq);
        if (!v) {
            log_warn("Could not serve a view (%d was requested)", view_id);
            return -1;
        }
    }

    v->active = true;
    log_info("Serving view %d (%d was requested)", v->id, view_id);
    return v->id;
}

void aq__yield_view(aquarium aq, int view_id) {
    log_info("Yielding view %d", view_id);

    view v = find_view(aq, view_id);
    if (v)
        v->active = false;
}

bool aq__del_view(aquarium aq, int view_id) {
    log_info("Deleting view %d", view_id);

    view v = find_view(aq, view_id);
    if (!v) {
        log_warn("Failed to delete view %d: it does not exist", view_id);
        return false;
    }

    pthread_mutex_lock(&aq->views_mutex);
    STAILQ_REMOVE(&aq->views, v, view, next);
    pthread_mutex_unlock(&aq->views_mutex);

    view__free(v);
    return true;
}