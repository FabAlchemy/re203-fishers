#include <stdlib.h>
#include <stdio.h>

#include "view.h"

view view__new(int id, int_pair pos, int_pair dim) {
    view v = malloc(sizeof(*v));
    v->id = id;
    v->pos = pos;
    v->dim = dim;
    v->active = false;

    return v;
}

void view__free(view v) {
    free(v);
}

void view__show(view v, FILE *stream) {
    fprintf(stream, "N%d %dx%d+%d+%d\n",
            v->id, v->pos.x, v->pos.y, v->dim.x, v->dim.y);
}