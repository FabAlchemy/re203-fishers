#ifndef AQUARIUM_H
#define AQUARIUM_H

#include <stdbool.h>
#include <pthread.h>

#include "../utils/queue.h"
#include "../utils/geometry.h"
#include "../utils/string.h"

/**
 * An opaque structure representing an aquarium
 */
typedef struct aquarium *aquarium;

/**
 * An aquarium could be summarized as a rectangle meant to store fishes and views
 */
struct aquarium {
    int_pair dim; /**< aquarium size */
    int time_interval; /**< interval of time for fishes to update their position */
    STAILQ_HEAD(fish_list, fish) fishes; /**< the tail queue containing the fishes */
    pthread_mutex_t fishes_mutex; /**< a mutex to protect the fish list */
    STAILQ_HEAD(view_list, view) views; /**< the tail queue containing the views */
    pthread_mutex_t views_mutex; /**< a mutex to protect the fish list */
    pthread_t updater; /**< the thread updating the aquarium */
    bool updating; /**< is the aquarium updating */
    pthread_mutex_t updating_mutex; /**< a mutex to protect the updating field */
    pthread_cond_t updating_cond; /**< a condition variable to stop sleeping early */
};


/**
 * Allocates memory for an aquarium
 * @note For now, arbitrary values are assigned to the width and height of the aquarium
 * @return an aquarium (a pointer to an allocated struct aquarium)
 */
aquarium aq__init(int time_interval);

/** Load topology (parse file) from the given file to the already instantiated aquarium aq */
int aq__load(aquarium aq, char *file);

/** Save current topology to given topology file */
int aq__save(aquarium aq, char *file);

/** Clean (remove fish and views) the aquarium */
void aq__clean(aquarium aq);

/** Frees memory allocated for the aquarium */
void aq__free(aquarium aq);

/** Getter for time_interval value */
int aq__get_time_interval(aquarium aq);

/**
 * Adds a new fish into a view of the aquarium, only if it not already in the aquarium
 * @return true on success, false otherwise
 */
bool aq__add_fish(aquarium aq, int view_id, char *name, int_pair pos, int_pair dim, char *behavior);

/**
 * Removes the fish f from the aquarium aq only if it is already in there
 * @return true on success, false otherwise
 */
bool aq__del_fish(aquarium aq, char *name);

/**
 * Starts the fish f from the aquarium aq only if it is already in there
 * @note if the fish is already started, then nothing changes
 * @return true on success, false otherwise
 */
bool aq__start_fish(aquarium aq, char *name);

/**
 * Adds a new view to the aquarium aq if it is not already existing
 * @return true on success, false otherwise
 */
bool aq__add_view(aquarium aq, int view_id, int_pair pos, int_pair dim);

/**
 * Requests an inactive view or finds the first available
 * @return a view_id on success, -1 otherwise
 */
int aq__request_view(aquarium aq, int view_id);

/**
 * Yield a view to make it available again
 * @param view_id the id of the view to yield
 */
void aq__yield_view(aquarium aq, int view_id);

/**
 * Deletes the view identified by view_id from the aquarium
 * @return true on success, false otherwise
 */
bool aq__del_view(aquarium aq, int view_id);

/**
 * Searches for all the fishes contained in the view
 * @return a concatenation of all the fishes with their position
 */
string aq__get_fishes_in_view(aquarium aq, int view_id);

/**
 * Start updating the aquarium
 */
void aq__start(aquarium aq);

/**
 * Stop the aquarium updating thread
 */
void aq__stop(aquarium aq);

/**
 * Updates positions of the started fishes
 * @param time_interval
 */
void aq__update(aquarium aq);

/** Prints the aquarium topology */
void aq__show(aquarium aq, FILE *stream);

/** Prints the name of the fishes of the aquarium */
void aq__print_fishes(aquarium aq);


#endif
