if (NO_RLWRAP)

    file(WRITE ${CMAKE_BINARY_DIR}/tmp/run
            "#!/usr/bin/env bash
            ./main $@")

else ()

    include(ExternalProject)

    ExternalProject_Add(gnu_readline
            URL https://git.savannah.gnu.org/cgit/readline.git/snapshot/readline-8.1.tar.gz
            SOURCE_DIR ${CMAKE_BINARY_DIR}/readline
            BINARY_DIR ${CMAKE_BINARY_DIR}/readline
            CONFIGURE_COMMAND ./configure --prefix=${CMAKE_BINARY_DIR}/readline
            BUILD_COMMAND make -j
            INSTALL_COMMAND make install
            )

    set(rlwrap_CPPFLAGS "-I${CMAKE_BINARY_DIR}/readline/include")
    set(rlwrap_LDFLAGS "-L${CMAKE_BINARY_DIR}/readline/lib/ -static")

    ExternalProject_Add(rlwrap
            URL https://github.com/hanslub42/rlwrap/archive/refs/tags/v0.45.1.tar.gz
            SOURCE_DIR ${CMAKE_BINARY_DIR}/rlwrap-src
            BINARY_DIR ${CMAKE_BINARY_DIR}/rlwrap-src
            CONFIGURE_COMMAND autoreconf || automake --add-missing && autoreconf
            && env CPPFLAGS=${rlwrap_CPPFLAGS} LDFLAGS=${rlwrap_LDFLAGS}
            ./configure --prefix=${CMAKE_BINARY_DIR}/rlwrap-src
            BUILD_COMMAND make -j
            INSTALL_COMMAND make install && cp ${CMAKE_BINARY_DIR}/rlwrap-src/bin/rlwrap ${CMAKE_BINARY_DIR}/rlwrap
            )

    add_dependencies(rlwrap gnu_readline)

    file(WRITE ${CMAKE_BINARY_DIR}/tmp/run
            "#!/usr/bin/env bash
            LC_ALL=C ./rlwrap -n ./main $@")

endif ()

file(COPY ${CMAKE_BINARY_DIR}/tmp/run
        DESTINATION ${CMAKE_BINARY_DIR}
        FILE_PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ)