#include <stdio.h>
#include <stdlib.h>
#include "parser.h"

int handler(__attribute__((unused)) void *entity, char **args) {
    if (args) {
        printf("[HANDLER] %s\n", args[0]);
        return 1;
    }
    printf("[HANDLER] (no argument)\n");
    return 0;
}

int main() {
    struct command hello_cmd[] = {
            {"command1 (.*)", 1, handler},
            {"command2",      0, handler},
    };

    parser p = parser__init(hello_cmd, 2);
    if (!p) return EXIT_FAILURE;

    char buffer[64] = {};
    scanf("%63[^\n]", buffer);

    int res = parser__parse(p, buffer, NULL);

    if (res == -1)
        printf("no matching command :/\n");

    parser__free(p);
}
