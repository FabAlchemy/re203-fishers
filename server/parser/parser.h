/**
 * @file parser.h
 * @brief A homemade command parser leveraging regular expressions.
 */

#ifndef FISHERS_PARSER_H
#define FISHERS_PARSER_H

#include <stdbool.h>

/**
 * An opaque structure representing a command parser
 */
typedef struct parser *parser;

/**
 * A type of function to handle commands
 */
typedef int (*command_handler)(void *entity, char **args);

/**
 * A command item to be parsed
 */
typedef struct command {
    char *pattern; /**< a regex describing the command */
    int arg_count; /**< the number of arguments expected */
    command_handler handler; /**< a function to handle the arguments */
} command;

/**
 * Free an array of strings
 * @param args the array to free
 * @param count the number of strings in the array
 */
void free_string_array(char **args, int count);


/**
 * Initialize the parser by compiling the regexes of a list of commands
 * @param commands a list of commands
 * @param count the number of commands
 * @return a new parser ready to parser__parse strings
 */
parser parser__init(const command *commands, int count);

/**
 * Try to match `str` against the list of compiled commands
 * then call the associated mock_handler.
 *
 * @param parser the parser to run
 * @param str the command to parse
 * @param args a pointer to a `char**` that will be allocated
 * @note you need to free `*args` after use
 * @return the result of the matching command function
 *         or -1 if the `str` does not match anything
 */
int parser__parse(parser p, const char *str, void *entity);

/**
 * Free the allocated parser
 * @param parser a parser
 */
void parser__free(parser p);

#endif // FISHERS_PARSER_H
