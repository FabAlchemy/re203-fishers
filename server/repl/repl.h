#ifndef FISHERS_REPL_H
#define FISHERS_REPL_H

#include <pthread.h>
#include <stdlib.h>

#include "../aquarium/aquarium.h"
#include "../network/server.h"
#include "../parser/parser.h"

#define BUFFER_SIZE 128

extern aquarium aq;
extern server serv;

/**
 * An opaque representation of a REPL
 * REPL = Read Eval Print Loop
 */
typedef struct repl *repl;

/** Initialize and start a REPL */
repl repl__start();

/** Join the REPL thread */
void repl__join(repl r);

/** Stop the REPL */
void repl__stop(repl r);

/** Free the memory allocated for the REPL */
void repl__free(repl r);


#endif // FISHERS_REPL_H
