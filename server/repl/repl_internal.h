#ifndef FISHERS_REPL_INTERNAL_H
#define FISHERS_REPL_INTERNAL_H

#include "repl.h"

/** The internal representation of a REPL */
struct repl {
    parser parser; /**< The command parser */
    pthread_t thread;
    pthread_mutex_t mutex;
    bool running;
};

/**
 * The REPL thread function
 * Read and parse lines read from STDIN
 */
void repl__thread(repl r);

#endif // FISHERS_REPL_INTERNAL_H
