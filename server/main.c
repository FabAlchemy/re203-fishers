#include <unistd.h>
#include <signal.h>

#include "config/config.h"
#include "repl/repl.h"
#include "log/log.h"
#include "log/log_setup.h"

aquarium aq;
server serv;
repl r;

__attribute__((destructor)) __attribute_used__ void clean_log_file() {
    log_free();
}

void free_memory() {
    log_warn("Shutting down the server");
    repl__free(r);
    server__free(serv);
    aq__free(aq);
}

/** Properly exit the server on SIGINT */
void signal_handler() {
    repl__stop(r);
    free_memory();
    exit(EXIT_SUCCESS);
}

int main(int argc, char **argv) {
    struct sigaction act = {.sa_handler = signal_handler};
    sigaction(SIGINT, &act, NULL);

    int verbosity = 0;
    int opt;
    while ((opt = getopt(argc, argv, "v")) != -1)
        if (opt == 'v')
            verbosity++;
    log_set_verbosity(verbosity);

    char *config_file = "controller.cfg";
    config c = config__init(config_file);

    if (!c) {
        log_fatal("Failure while parsing configuration, exiting.");
        return EXIT_FAILURE;
    }

    char *log_filename = config__get_value(c, "log-filename");
    log_set_file(log_filename);

    aq = aq__init(strtoi(config__get_value(c, "fish-update-interval")));
    aq__load(aq, config__get_value(c, "topology-file"));
    aq__start(aq);

    serv = server__init(strtoi(config__get_value(c, "controller-port")),
                        strtoi(config__get_value(c, "display-timeout-value")));
    server__start(serv);

    config__free(c);

    r = repl__start();
    repl__join(r); // Blocking call
    free_memory();
}