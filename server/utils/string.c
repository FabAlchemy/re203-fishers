#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <limits.h>

#include "string.h"

string string__new(char *content, size_t length) {
    string s = calloc(1, sizeof(*s));
    s->content = content;
    s->length = length;
    return s;
}

string string__from(const char* str) {
    return alloc_printf("%s", str);
}

string alloc_printf(const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    size_t size = vsnprintf(NULL, 0, fmt, ap);
    va_end(ap);

    char *buffer = calloc(1, size + 1);
    if (!buffer) {
        return NULL;
    }

    va_start(ap, fmt);
    vsnprintf(buffer, size + 1, fmt, ap);
    va_end(ap);

    return string__new(buffer, size);
}

void string__free(string s) {
    free(s->content);
    free(s);
}

/** The list of characters to escape */
static const char escaped_char[] = {'\n', '\t', '\r'};

/** The corresponding escaped strings */
static const char escaped_symbol[] = {'n', 't', 'r'};

/** The number of characters to escape */
static const int escape_count = sizeof(escaped_char) / sizeof(escaped_char[0]);

string escape_string(const char *str) {
    size_t len = strlen(str);
    char *res = calloc(2*len + 1, sizeof(char));
    char *res_beg = res;

    while (*str != '\0') {
        bool found_token = false;

        for (int i = 0; i < escape_count; i++)
            if (*str == escaped_char[i]) {
                *res++ = '\\';
                *res++ = escaped_symbol[i];
                found_token = true;
                len++;
                break;
            }

        if (!found_token)
            *res++ = *str;

        str++;
    }

    res_beg = realloc(res_beg, len + 1);
    return string__new(res_beg, len);
}

int strtoi(const char *str) {
    errno = 0;

    long res = strtol(str, NULL, 10);
    if (errno)
        return 0;

    if (res < INT_MIN || res > INT_MAX) {
        errno = ERANGE;
        return 0;
    }

    return (int) res;
}
