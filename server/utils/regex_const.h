#ifndef FISHERS_REGEX_H
#define FISHERS_REGEX_H

#define BLANK       "[[:blank:]]"
#define OPT_SPACE   BLANK "*"
#define SPACE       BLANK "+"

#define NUMBER      "([[:digit:]]+)"
#define STRING      "([[:alnum:]]+)"
#define COORDS_SEP  "x"
#define COMMA       ","
#define SEPARATOR   OPT_SPACE COMMA OPT_SPACE
#define COORDS      NUMBER OPT_SPACE COORDS_SEP OPT_SPACE NUMBER
#define END         OPT_SPACE

#endif // FISHERS_REGEX_H
