#ifndef FISHERS_STRING_H_H
#define FISHERS_STRING_H_H

#include <stdio.h>
#include "queue.h"

typedef struct string *string;

/** A structure to help manipulate strings */
struct string {
    char *content;
    size_t length;
    STAILQ_ENTRY(string) next;
};

/**
 * Allocate a string helper
 * @param content malloc-ed content of the string
 * @param length the length of the string
 * @note length does _not_ count the ending \0
 * @return a pointer to an allocated string helper
 */
string string__new(char *content, size_t length);

/**
 * Copy a string on the stack and allocate a string helper
 * @param str the string to copy
 * @return a pointer to a string helper
 */
string string__from(const char* str);

/**
 * Print to specially allocated string helper
 * @param fmt a format string
 * @param ... the arguments to print in the string
 * @return The allocated string helper
 * @note Re-implementation of the same name non-POSIX GNU function
 */
string alloc_printf(const char *fmt, ...);

/** Free a string helper and its content */
void string__free(string s);

/**
 * Escape special characters in a string
 * @param str the string to escape
 */
string escape_string(const char *str);

/**
 * Converts a string to an integer
 * @param the string to convert
 * @return an integer
 */
int strtoi(const char *str);

#endif // FISHERS_STRING_H_H
