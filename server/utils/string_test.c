#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdlib.h>
#include "string.h"

static void string__prints_expected_content(__attribute__((unused)) void **state) {
    string str = alloc_printf("Hello %s - %d", "abc", 5);
    assert_string_equal(str->content, "Hello abc - 5");
    string__free(str);
}

static void string__accepts_zero_arguments(__attribute__((unused)) void **state) {
    string str = alloc_printf("Hello\n");
    assert_string_equal(str->content, "Hello\n");
    string__free(str);
}

static void string__escape_string(__attribute__((unused)) void **state) {
    char buffer[20] = {};
    FILE *f = fmemopen(buffer, sizeof(buffer), "w");
    string res = escape_string("abc\ndef\t");
    fclose(f);

    assert_string_equal(res->content, "abc\\ndef\\t");
    string__free(res);
}

int main() {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(string__prints_expected_content),
            cmocka_unit_test(string__accepts_zero_arguments),
            cmocka_unit_test(string__escape_string),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}