#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "geometry.h"

/** TEST: Should not fail with non overlapping rectangles */
static void rectangle__detects_non_overlapping_rectangles(__attribute__((unused)) void **state) {
    rectangle R = rectangle__new((int_pair) {10, 10},
                                 (int_pair) {20, 20});

    rectangle r1 = rectangle__new((int_pair) {5, 5},
                                  (int_pair) {7, 15});
    assert_true(rectangle__is_intersection_empty(r1, R));
    assert_true(rectangle__is_intersection_empty(R, r1));

    rectangle r2 = rectangle__new((int_pair) {5, 5},
                                  (int_pair) {15, 7});
    assert_true(rectangle__is_intersection_empty(r2, R));
    assert_true(rectangle__is_intersection_empty(R, r2));

    rectangle r3 = rectangle__new((int_pair) {17, 22},
                                  (int_pair) {25, 25});
    assert_true(rectangle__is_intersection_empty(r3, R));
    assert_true(rectangle__is_intersection_empty(R, r3));

    rectangle r4 = rectangle__new((int_pair) {22, 17},
                                  (int_pair) {25, 25});
    assert_true(rectangle__is_intersection_empty(r4, R));
    assert_true(rectangle__is_intersection_empty(R, r4));
}

/** TEST: Should be able to detect overlapping rectangles */
static void rectangle__detects_overlapping_rectangles(__attribute__((unused)) void **state) {
    rectangle R = rectangle__new((int_pair) {10, 10},
                                 (int_pair) {20, 20});

    rectangle r1 = rectangle__new((int_pair) {5, 5},
                                  (int_pair) {15, 15});
    assert_false(rectangle__is_intersection_empty(r1, R));
    assert_false(rectangle__is_intersection_empty(R, r1));

    rectangle r2 = rectangle__new((int_pair) {15, 16},
                                  (int_pair) {25, 18});
    assert_false(rectangle__is_intersection_empty(r2, R));
    assert_false(rectangle__is_intersection_empty(R, r2));

    // Works with badly-shaped rectangle declaration
    rectangle r3 = rectangle__new((int_pair) {5, 15},
                                  (int_pair) {15, 5});
    assert_false(rectangle__is_intersection_empty(r3, R));
    assert_false(rectangle__is_intersection_empty(R, r3));

    rectangle r4 = rectangle__new((int_pair) {15, 15},
                                  (int_pair) {20, 5});
    assert_false(rectangle__is_intersection_empty(r4, R));
    assert_false(rectangle__is_intersection_empty(R, r4));
}

/** TEST: Overlapping rectangles should intersect correctly */
static void rectangle__correct_intersection(__attribute__((__unused__)) void **state) {
    rectangle r1 = {(int_pair) {5, 5}, (int_pair) {15, 15}};
    rectangle r2 = {(int_pair) {10, 10}, (int_pair) {20, 20}};

    rectangle r3 = rectangle__intersection(r1, r2);
    assert_true(int_pair__equal(r3.tl, (int_pair) {10, 10}));
    assert_true(int_pair__equal(r3.br, (int_pair) {15, 15}));
}

/** TEST: Non-overlapping rectangles should intersect into a reversed rectangle */
static void rectangle__empty_intersection(__attribute__((__unused__)) void **state) {
    rectangle r1 = {(int_pair) {5, 5}, (int_pair) {15, 15}};
    rectangle r2 = {(int_pair) {20, 20}, (int_pair) {30, 30}};

    rectangle r3 = rectangle__intersection(r1, r2);
    assert_true(int_pair__is_smaller(r3.br, r3.tl));
}

/** TEST: A random point should be in the rectangle */
static void rectangle__random_point_is_inside(__attribute__((__unused__)) void **state) {
    rectangle r1 = {(int_pair) {0, 0}, (int_pair) {10, 10}};
    int_pair pos = rectangle__random_point(r1);
    assert_in_range(pos.x, 0, 10);
    assert_in_range(pos.y, 0, 10);

    rectangle r2 = {(int_pair) {10, 10}, (int_pair) {20, 20}};
    int_pair pos2 = rectangle__random_point(r2);
    assert_in_range(pos2.x, 10, 20);
    assert_in_range(pos2.y, 10, 20);
}

/** TEST: A halo should be correctly computed */
static void int_pair__computes_halo(__attribute__((__unused__)) void **state) {
    int_pair pos = {10, 10};
    rectangle r = int_pair__halo(pos, 5);

    assert_true(int_pair__equal(r.tl, (int_pair) {5, 5}));
    assert_true(int_pair__equal(r.br, (int_pair) {15, 15}));
}

int main() {
    const struct CMUnitTest tests[] = {
            cmocka_unit_test(rectangle__detects_non_overlapping_rectangles),
            cmocka_unit_test(rectangle__detects_overlapping_rectangles),
            cmocka_unit_test(rectangle__correct_intersection),
            cmocka_unit_test(rectangle__empty_intersection),
            cmocka_unit_test(rectangle__random_point_is_inside),
            cmocka_unit_test(int_pair__computes_halo),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}