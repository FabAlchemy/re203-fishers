# RE203-Fishers

A centralized fish aquarium.

A Makefile has been written to simplify the compilation and the execution.

- `make` or `make download`: Downloads the latest binaries for client and server and decompresses them in the correct folders

- `make build`: Compiles the client and the server locally

    - *Requirements for the server* : 
    `cmake-3.+`,`git`, `gcc`, `make`, `wget`, `autoreconf`, `automake`, `glibc-static`, `ncurses-devel ncurses-static`
    - *Requirements for the client* :
    `openjdk-16-jdk`
      

- `make install`: Installs compiled binaries into correct folders

- `make test`: Runs the tests for server

- `make valgrind`: Runs the tests for server with valgrind

- `make server`: Runs the server

- `make client`: Runs the client

- `make clean`: Deletes compiled binaries and utility folders


Download links for packaged versions:

- [Server (Linux x64)](https://gitlab.com/FabAlchemy/re203-fishers/-/jobs/artifacts/master/raw/server-linux-x64.zip?job=deploy)
- [Client (Linux x64)](https://gitlab.com/FabAlchemy/re203-fishers/-/jobs/artifacts/master/raw/client-linux-x64.zip?job=deploy)
- [Client (Windows x64)](https://gitlab.com/FabAlchemy/re203-fishers/-/jobs/artifacts/master/raw/client-windows-x64.zip?job=deploy)
