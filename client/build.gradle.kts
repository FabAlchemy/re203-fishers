plugins {
    id("application")
    id("org.openjfx.javafxplugin") version "0.0.8"
    id("org.beryx.jlink") version "2.23.8"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.aesh:aesh-readline:1.17")
}

javafx {
    version = "13"
    modules = listOf("javafx.controls", "javafx.fxml")
}

jlink {
    launcher {
        name = "client"
    }

    if (System.getProperty("os.name").contains("Win")) {
        jpackage {
            imageOptions = listOf("--win-console")
        }
    }
}

application {
    mainClass.set("client.Main")
}

tasks.getByName<JavaExec>("run") {
    standardInput = System.`in`
}

tasks.getByName("jpackageImage") {
    doLast {
        if (System.getProperty("os.name").contains("Win")) {
            File("./resources").copyRecursively(File("./build/jpackage/client/resources"))
        } else {
            File("./resources").copyRecursively(File("./build/jpackage/client/bin/resources"))
        }
    }
}