package client;

import client.Fishes.FishList;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class FxController {

	/**
	 * Main AnchorPane
	 */
	private static AnchorPane anchorPane;

	/**
	 * Sets primaryStage and initializes listeners for window resizing and maximizing
	 *
	 * @param primaryStage The primary stage
	 */
	public static void setPrimaryStage(Stage primaryStage) {
		FxController.anchorPane = (AnchorPane) primaryStage.getScene().lookup("#AnchorPane");
		adaptFishes(new ArrayList<>(List.of(
				primaryStage.heightProperty(),
				primaryStage.widthProperty())));
	}

	/**
	 * Adds EventListeners to specified properties.
	 *
	 * @param doubleProperties Resizing properties (width and height)
	 */
	private static void adaptFishes(ArrayList<ReadOnlyDoubleProperty> doubleProperties) {
		doubleProperties.forEach(property -> property.addListener((obs, oldVal, newVal) -> {
			FishList.fishList.forEach((fishName, fish) -> fish.stopCurrentTransition());
			getAnchorPane().getChildren().clear();
			FishList.fishList.clear();
		}));
	}

	public static AnchorPane getAnchorPane() {
		return anchorPane;
	}
}
