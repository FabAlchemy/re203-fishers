package client.UserCommands;

import client.InternalProtocol;

public class Logout implements UserCommands {
	@Override
	public boolean shouldWaitForAnswer() {
		return false;
	}

	@Override
	public boolean verifySyntax(String[] ArgumentArray) {
		return ArgumentArray.length == 2;
	}

	@Override
	public String[] translate(String[] ArgumentArray) {
		return new String[]{
				InternalProtocol.logout()
		};
	}

	@Override
	public String usage() {
		return "Usage : log out";
	}

	@Override
	public boolean verifyResponse(String Response) {
		return false;
	}

	@Override
	public String NOKResponse() {
		return "";
	}

	@Override
	public String OKResponse() {
		return "";
	}

	@Override
	public void actionOnAnswer() {

	}
}
