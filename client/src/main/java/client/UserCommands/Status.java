package client.UserCommands;

import client.Fishes.FishList;

public class Status implements UserCommands {
	@Override
	public boolean shouldWaitForAnswer() {
		return false;
	}

	@Override
	public boolean verifySyntax(String[] ArgumentArray) {
		return ArgumentArray.length == 1;
	}

	@Override
	public String[] translate(String[] ArgumentArray) {
		return new String[0];
	}

	@Override
	public String usage() {
		return "Usage : status";
	}

	@Override
	public boolean verifyResponse(String Response) {
		return true;
	}

	@Override
	public String NOKResponse() {
		return "";
	}

	@Override
	public String OKResponse() {
		StringBuffer OKResponse = new StringBuffer("OK : connecté au contrôleur, " + FishList.fishList.size() + " poisson(s) trouvé(s)\n");
		FishList.fishList.forEach((key, value) -> {
			String newline = value.toString();
			OKResponse.append(newline);
		});
		return OKResponse.toString();
	}

	@Override
	public void actionOnAnswer() {
	}
}
