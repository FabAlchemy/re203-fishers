package client.UserCommands;

import client.InternalProtocol;

import java.util.regex.Pattern;

public class AddFish implements UserCommands {

	@Override
	public boolean shouldWaitForAnswer() {
		return true;
	}

	@Override
	public boolean verifySyntax(String[] ArgumentArray) {
		return ArgumentArray.length == 6 && Pattern.matches("[0-9]+x[0-9]+", ArgumentArray[3]) && Pattern.matches("[0-9]+x[0-9]+", ArgumentArray[4]);
	}

	@Override
	public String[] translate(String[] ArgumentArray) {
		return new String[]{InternalProtocol.addFish(ArgumentArray[1], ArgumentArray[3], ArgumentArray[4], ArgumentArray[5])};
	}

	@Override
	public String usage() {
		return "Usage : addFish [FishName] at [FishPosition],[FishSize], [FishMovement]  \n Example : addFish PoissonNain at 61x52,4x3, RandomWayPoint";
	}

	@Override
	public boolean verifyResponse(String Response) {
		return Response.equals("OK") || Response.equals("NOK");
	}

	@Override
	public String NOKResponse() {
		return "NOK : mobility model not supported or existing fish";
	}

	@Override
	public String OKResponse() {
		return "OK";
	}

	@Override
	public void actionOnAnswer() {
	}
}
