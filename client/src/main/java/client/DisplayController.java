package client;

import client.UserCommands.UserCommands;

import java.io.IOException;

public class DisplayController {

	/**
	 * Path to the external resources folder
	 */
	private static final String resourcesPath = "./resources";

	/**
	 * Path to the config file
	 */
	private static final String configPath = "affichage.cfg";

	/**
	 * Config class used to access the value of the config file
	 */
	private static Config configValues = null;

	/**
	 * Utility class used to output in terminal
	 */
	final UserOutput userOutput;

	/**
	 * Processes server messages
	 */
	ServerResponseProcessor serverResponseProcessor;

	/**
	 * Socket instance used to communicate with server
	 */
	ClientSocket socket;

	/**
	 * Current instance of client.UserCommands (from the last command)
	 */
	UserCommands currentCommandInstance;

	/**
	 * ID of the client display
	 */
	private String DisplayId;

	private boolean isWaitingForAnswer = false;

	private boolean isGreeted = false;

	public DisplayController() {
		userOutput = new UserOutput(this);
		configValues = new Config(getPath(configPath), this);
		Log.initializeLogger(this);
		UserInputProcessor userInputProcessor = new UserInputProcessor(this);
		userInputProcessor.start();
	}

	/**
	 * Returns the corresponding value to id in the config file
	 *
	 * @param id The id of the field
	 * @return The value in the config file
	 */
	public static String getConfigValue(Config.Value id) {
		if (configValues != null) {
			return configValues.getProperty(id);
		} else {
			throw new RuntimeException("Config file not initialized");
		}
	}

	/**
	 * Gets the relative path of a file to the src directory
	 *
	 * @param path PAth of the file in the resources folder
	 * @return The complete relative path of the file to src
	 */
	public static String getPath(String path) {
		return resourcesPath + "/" + path;
	}

	/**
	 * Actions to do when the terminal is started
	 */
	public void onTerminalStarted() {
		makeConnection();
		serverResponseProcessor = new ServerResponseProcessor(this);
	}

	/**
	 * Starts socket connection
	 */
	private void makeConnection() {
		String hostname = getConfigValue(Config.Value.ADDRESS);
		int port = Integer.parseInt(getConfigValue(Config.Value.PORT));
		try {
			socket = new ClientSocket(this, hostname, port, getConfigValue(Config.Value.ID), Integer.parseInt(getConfigValue(Config.Value.TIMEOUT)), Integer.parseInt(getConfigValue(Config.Value.MAX_PING_RETRY)));
			socket.start();
		} catch (IOException e) {
			Log.error("No server found at " + hostname + ":" + port);
			System.exit(0);
		}
	}

	public boolean isGreeted() {
		return isGreeted;
	}

	public void setGreeted(boolean greeted) {
		isGreeted = greeted;
	}

	public boolean isWaitingForAnswer() {
		return isWaitingForAnswer;
	}

	public void setWaitingForAnswer(boolean waitingForAnswer) {
		isWaitingForAnswer = waitingForAnswer;
	}

	public String getDisplayId() {
		return DisplayId;
	}

	public void setDisplayId(String displayId) {
		DisplayId = displayId;
	}
}
