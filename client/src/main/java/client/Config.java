package client;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;


public class Config {
	/**
	 * Property list with each key and its corresponding value in the config file
	 */
	final Properties configFile;

	/**
	 * Initialize an instance of Config
	 *
	 * @param path of the config file
	 */
	public Config(String path, DisplayController displayController) {
		configFile = new java.util.Properties();
		InputStream file = null;
		try {
			file = new FileInputStream(path);
		} catch (FileNotFoundException e) {
			displayController.userOutput.displayError("Configuration file not found");
			System.exit(1);
		}
		try {
			configFile.load(file);
		} catch (Exception eta) {
			eta.printStackTrace();
		}
	}

	/**
	 * Return the value corresponding to the key
	 *
	 * @param key corresponding to the configuration parameter we want
	 * @return A string which indicates the value of the key in the config file
	 */
	public String getProperty(Value key) {
		return this.configFile.getProperty(key.getName());
	}

	/**
	 * Enum of all config values.
	 */
	public enum Value {
		ADDRESS("controller-address"),
		ID("id"),
		PORT("controller-port"),
		TIMEOUT("display-timeout-value"),
		MAX_PING_RETRY("max-ping-retry"),
		RESOURCES("resources"),
		DEFAULT_FISH_IMAGE("default-fish-image"),
		LOG_FILENAME("log-filename"),
		VERBOSITY("verbosity");

		/**
		 * Name of the parameter in the config file
		 */
		private final String name;

		Value(String name) {
			this.name = name;
		}

		public String getName() {
			return this.name;
		}

	}

}

