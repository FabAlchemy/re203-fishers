package client;

public class InternalProtocol {

	/**
	 * @return Internal ping command
	 */
	public static String ping(String id) {
		return "ping " + id;
	}

	/**
	 * @param id Display module id
	 * @return Internal hello command
	 */
	public static String hello(String id) {
		if (id.equals("")) {
			return "hello";
		}
		return "hello in as " + id;
	}

	/**
	 * @param fish Name of the fish
	 * @return Internal startFish command
	 */
	public static String startFish(String fish) {
		return "startFish " + fish;
	}

	public static String addFish(String fish, String position, String size, String movement) {
		return "addFish " + fish + " at " + position + "," + size + ", " + movement;
	}

	public static String delFish(String fish) {
		return "delFish " + fish;
	}

	@SuppressWarnings("SameReturnValue")
	public static String getFishes() {
		return "getFishes";
	}

	@SuppressWarnings("SameReturnValue")
	public static String getFishesContinuously() {
		return "getFishesContinuously";
	}

	@SuppressWarnings("SameReturnValue")
	public static String logout() {
		return "log out";
	}
}
