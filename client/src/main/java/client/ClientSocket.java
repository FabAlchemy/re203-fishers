package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

public class ClientSocket extends Thread {

	/**
	 * Socket used to connect to server
	 */
	private final Socket socket;

	/**
	 * Display instance
	 */
	private final DisplayController displayController;

	/**
	 * Interval of ping
	 */
	private final int pingInterval;

	/**
	 * Display id
	 */
	private final String id;

	/**
	 * PrintWriter used to send messages to server
	 */
	private final PrintWriter out;

	/**
	 * BufferedReader user to read messages from server
	 */
	private final BufferedReader in;

	/**
	 * Maximum retry number of ping
	 */
	private final int maxRetryNumber;

	/**
	 * Tells if we are waiting for a pong
	 */
	private boolean isWaitingForPong = false;

	/**
	 * Current retry number of ping
	 */
	private int retryNumber = 0;

	/**
	 * Tells if the disconnection process has already been triggered
	 */
	private boolean hasAlreadyBeenDisconnected = false;


	public ClientSocket(DisplayController displayController, String hostname, int port, String id, int pingInterval, int maxRetryNumber) throws IOException {
		this.displayController = displayController;
		this.pingInterval = pingInterval;
		this.maxRetryNumber = maxRetryNumber;
		this.id = id;
		socket = new Socket(hostname, port);
		out = new PrintWriter(socket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	}

	/**
	 * Starts a new thread.
	 * Ask for authentication, starts pinging the server and listens for new messages
	 */
	public void run() {
		authenticate(id);
		startPing();
		receive();
	}

	/**
	 * Makes the authentication of display
	 *
	 * @param id Display identifier
	 */
	private void authenticate(String id) {
		send(InternalProtocol.hello(id));
	}

	/**
	 * Sends the specified message to server
	 *
	 * @param message The message to send
	 */
	public void send(String message) {
		out.println(message);
	}

	/**
	 * Starts reception of messages from the server
	 * Stops when the server is disconnected
	 */
	private void receive() {
		Thread t = new Thread(() -> {
			String outputFromServer;
			Log.info("Connecté au serveur !", true);
			while (isConnected()) {
				try {
					if ((outputFromServer = in.readLine()) == null) {
						break;
					}
					displayController.serverResponseProcessor.onReception(outputFromServer);
				} catch (IOException e) {
					Log.warning("Server is unreachable !");
				}
			}
			propagateDisconnection();
		});
		t.start();
	}

	/**
	 * Starts pinging server every pingInterval seconds.
	 */
	private void startPing() {
		new Timer().scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				String displayId = displayController.getDisplayId();
				if (isWaitingForPong) {
					retryNumber++;
					send(InternalProtocol.ping(displayId));
					Log.warning("Server did not respond to ping [" + retryNumber + " of " + maxRetryNumber + "]");
					if (retryNumber > maxRetryNumber) {
						propagateDisconnection();
					}
				} else {
					if (displayId != null) {
						send(InternalProtocol.ping(displayId));
						setWaitingForPong(true);
					}
				}
			}
		}, 0, pingInterval);
	}

	/**
	 * Propagates the disconnection event
	 */
	public void propagateDisconnection() {
		if (!hasAlreadyBeenDisconnected) {
			hasAlreadyBeenDisconnected = true;
			try {
				disconnect();
			} catch (IOException e) {
				Log.warning("Socket connection has already been closed");
			}
			try {
				in.close();
			} catch (IOException e) {
				Log.warning("No input to close");
			}
			out.close();
			Log.warning("Server is disconnected");
			System.exit(0);
		}
	}

	/**
	 * Disconnects socket from server
	 *
	 * @throws IOException Throws a IOException if disconnection fails.
	 */
	public void disconnect() throws IOException {
		socket.shutdownInput();
		socket.shutdownOutput();
		socket.close();
	}

	public boolean isConnected() {
		return socket.isConnected() && !socket.isClosed();
	}

	public void setWaitingForPong(boolean waitingForPong) {
		isWaitingForPong = waitingForPong;
	}

	public void setRetryNumber(int retryNumber) {
		this.retryNumber = retryNumber;
	}
}
