package client.Fishes;

import client.FxController;
import javafx.scene.layout.AnchorPane;

public class FishGeometry {

	/**
	 * Percent coordinates of the fish
	 */
	private final Coordinates percentPosition;

	/**
	 * Percent size of the fish
	 */
	private final Coordinates percentSize;

	/**
	 * Scaled coordinates of the fish
	 */
	private Coordinates position;

	/**
	 * Scaled size of the fish
	 */
	private Coordinates size;

	public FishGeometry(Coordinates percentCoordinates, Coordinates percentSize) {
		this.percentPosition = percentCoordinates;
		this.percentSize = percentSize;
		this.scale();
	}

	/**
	 * Scales coordinates to user screen
	 *
	 * @param percentCoordinates Coordinates in percentage
	 * @return A scaled Coordinates instance
	 */
	private Coordinates scaleCoordinates(Coordinates percentCoordinates) {
		AnchorPane anchorPane = FxController.getAnchorPane();
		int x = (int) percentCoordinates.getX();
		int y = (int) percentCoordinates.getY();
		double scaledX = (double) x / 100;
		double scaledY = (double) y / 100;
		return new Coordinates(scaledX * anchorPane.getWidth(), scaledY * anchorPane.getHeight());
	}

	/**
	 * Scales current position and size.
	 */
	public void scale() {
		this.position = this.scaleCoordinates(percentPosition);
		this.size = this.scaleCoordinates(percentSize);
	}

	public double getX() {
		return (double) this.position.getX();
	}

	public double getY() {
		return (double) this.position.getY();
	}

	public double getSizeX() {
		return (double) this.size.getX();
	}

	public double getSizeY() {
		return (double) this.size.getY();
	}

	public Number getPercentX() {
		return percentPosition.getX();
	}

	public Number getPercentY() {
		return percentPosition.getY();
	}

	public Number getPercentSizeX() {
		return percentSize.getX();
	}

	public Number getPercentSizeY() {
		return percentSize.getY();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		FishGeometry that = (FishGeometry) o;
		return position.equals(that.position) && size.equals(that.size) && percentPosition.equals(that.percentPosition) && percentSize.equals(that.percentSize);
	}
}
