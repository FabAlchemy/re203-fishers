package client.Fishes;

/**
 * Possible status for a fish
 */
public enum startedStatus {
	STARTED, NOT_STARTED
}
