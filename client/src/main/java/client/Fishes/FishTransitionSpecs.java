package client.Fishes;

class FishTransitionSpecs {

	/**
	 * Describes the final fish geometry (after the transition)
	 */
	final FishGeometry finalGeometry;

	/**
	 * The duration of the movement
	 */
	final int duration;

	public FishTransitionSpecs(FishGeometry position, int delay) {
		this.finalGeometry = position;
		this.duration = delay;
	}
}
