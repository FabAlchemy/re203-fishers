package client.Fishes;

import javafx.application.Platform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FishList {

	/**
	 * Map of currently displayed fish (indexed by their name)
	 */
	public static final HashMap<String, Fish> fishList = new HashMap<>();

	/**
	 * A regexp used to split list command into fish specs
	 */
	private final static String regexp = "\\[(.*?)]";

	/**
	 * The compiled pattern used to split list command into fish specs
	 */
	private final static Pattern pattern = Pattern.compile(regexp);

	private static final ArrayList<String> currentReceivedFishList = new ArrayList<>();

	/**
	 * Splits the received list into transition spec.
	 * Calls process function for each transition spec.
	 *
	 * @param listCommand The received list command (example: "list [PoissonRouge at 40x40,30x30,5] [Dori at 50x80,30x30,5]").
	 */
	public static void splitList(String listCommand) {
		Matcher matcher = pattern.matcher(listCommand);
		while (matcher.find()) {
			processReceivedFish(matcher.group(1));
		}
		deleteUnusedFishes();
		currentReceivedFishList.clear();
	}

	/**
	 * Processes a received fish.
	 * Creates it on the pane if it does not exist.
	 * Updates it if it already exists.
	 *
	 * @param command The received command for this fish (example: "PoissonRouge at 40x40,30x30,5").
	 */
	private static void processReceivedFish(String command) {
		String[] commandTokens = command.split("[, ]");
		String fishName = commandTokens[0];
		currentReceivedFishList.add(fishName);
		Fish fish;
		if (fishList.containsKey(fishName)) {
			fish = fishList.get(fishName);
			fish.updateFish(commandTokens);
		} else {
			fish = Fish.createFish(commandTokens);
			if (fish != null) {
				fishList.put(fishName, fish);
				Platform.runLater(fish::firstDisplay);
			}
		}
	}

	/**
	 * Removes fishes that were not received in the list (from display panel and HashMap)
	 */
	private static void deleteUnusedFishes() {
		HashSet<String> toHideFishes = new HashSet<>(fishList.keySet());
		currentReceivedFishList.forEach(toHideFishes::remove);
		toHideFishes.forEach(fish -> Platform.runLater(fishList.get(fish)::removeFish));
		fishList.keySet().retainAll(currentReceivedFishList);
	}
}
