package client.Fishes;

import java.util.Objects;

@SuppressWarnings("ClassCanBeRecord")
public class Coordinates {

	private final Number x;

	private final Number y;

	public Coordinates(Number x, Number y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Deserializes coordinates and creates a Coordinates instance.
	 *
	 * @param coordinates A string representing coordinates (example : "80x10")
	 * @return A coordinates instance
	 */
	static Coordinates deserializeCoordinates(String coordinates) {
		String[] tokens = coordinates.split("x");
		if (tokens.length == 2) {
			return new Coordinates(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]));
		}
		return null;
	}

	public Number getX() {
		return x;
	}

	public Number getY() {
		return y;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Coordinates that = (Coordinates) o;
		return Objects.equals(x, that.x) && Objects.equals(y, that.y);
	}
}
