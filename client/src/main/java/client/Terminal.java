package client;

import org.aesh.readline.Prompt;
import org.aesh.readline.Readline;
import org.aesh.terminal.Connection;
import org.aesh.utils.ANSI;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class Terminal implements Consumer<Connection> {

	/**
	 * Connection to user terminal
	 */
	public static Connection connection;

	/**
	 * Default prompt line
	 */
	private static Prompt defaultPrompt;

	/**
	 * Readline object
	 */
	private static Readline readline;

	/**
	 * List of preprocessors used to process input commands
	 */
	private static List<Function<String, Optional<String>>> preProcessors;

	/**
	 * UserInputProcessor to which commands are transmitted
	 */
	private static UserInputProcessor inputProcessor;

	public Terminal(UserInputProcessor inputProcessor) {
		Terminal.inputProcessor = inputProcessor;
	}

	/**
	 * Displays a prompt and reads a line
	 *
	 * @param connection Terminal connection
	 * @param readline   Readline object
	 * @param prompt     Prompt line
	 */
	private static void readInput(Connection connection, Readline readline, Prompt prompt) {
		readline.readline(connection, prompt, line -> {
			if (line == null) {
				connection.close();
			} else if (line.equals("clear")) {
				connection.stdoutHandler().accept(ANSI.CLEAR_SCREEN);
				readInput(connection, readline, defaultPrompt);
			} else {
				Terminal.inputProcessor.processUserInput(line);
			}
		}, null, preProcessors);
	}

	/**
	 * Shows a prompt (for external classes)
	 */
	public static void startReading() {
		if (readline != null) {
			try {
				readInput(connection, readline, defaultPrompt);
			} catch (Exception ignored) {
			}
		}
	}

	/**
	 * Creates default user defined prompt
	 *
	 * @return A prompt
	 */
	private static Prompt createDefaultPrompt() {
		return new Prompt("[RE203 - Fishers] > ");

	}

	@Override
	public void accept(Connection connection) {
		Terminal.connection = connection;
		defaultPrompt = createDefaultPrompt();
		preProcessors = new ArrayList<>();
		readline = new Readline();
		inputProcessor.displayController.onTerminalStarted();
		connection.openBlocking();
	}
}
