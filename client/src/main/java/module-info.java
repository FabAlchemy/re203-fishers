module client {
	// Require JavaFX
	requires javafx.controls;
	requires javafx.fxml;
	requires java.logging;
	requires aesh.readline;

	// Export the "gui" package (needed by JavaFX to start the Application)
	// Replace "exports" with "opens" if "@FXML" is used in this module
	opens client;
}