SERVER=server/build-dir
INSTALL=install
CLIENT=client/build
JPACKAGE=client/build/jpackage/client

FILE = main run rlwrap aquarium1 aquarium2 controller.cfg

all: download

download: 
	./download.sh

build:
	mkdir -p $(SERVER) && cd $(SERVER) && cmake .. && make -j
	cd client && chmod +x gradlew && ./gradlew jpackageImage

install:
	mkdir -p $(INSTALL) && for file in $(FILE); do cp $(SERVER)/$$file $(INSTALL)/ ; done
	cp -r $(JPACKAGE) $(INSTALL)/client

server: install
	 cd $(INSTALL)/ && ./run

client: install
	cd ./install/client/bin/ && ./client

test:
	cd $(SERVER) && ctest

valgrind:
	cd $(SERVER) && for test in `find -name *_test`; do echo "\n ===== $$test ===== \n" && valgrind --leak-check=full --error-exitcode=-1 $$test; done

clean:
	rm -rf $(SERVER) $(INSTALL) $(CLIENT)
