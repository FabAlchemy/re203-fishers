B0;10;1c#/bin/env bash

INSTALL="install"
GL_ARTIFACTS="https://gitlab.com/FabAlchemy/re203-fishers/-/jobs/artifacts/master/raw"
GL_SUFFIX="?job=deploy"

mkdir -p $INSTALL
cd $INSTALL
wget $GL_ARTIFACTS/client-linux-x64.zip/$GL_SUFFIX -O client.zip
wget $GL_ARTIFACTS/server-linux-x64.zip/$GL_SUFFIX -O server.zip
unzip client.zip -d client
unzip server.zip -d .
rm -rf client.zip server.zip
